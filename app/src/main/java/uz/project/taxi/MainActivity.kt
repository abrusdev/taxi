package uz.project.taxi

import android.R.attr.data
import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.animation.*
import android.view.animation.Animation
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability


class MainActivity : AppCompatActivity() {

    private var TAG = "MainActivity"

    private var ERROR_DIALOG_REQUEST = 9001;
    private lateinit var recyclerView: RecyclerView
    private var items: ArrayList<TripItem> = ArrayList()

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        init()
        return super.onOptionsItemSelected(item)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back_white)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)


        initItems()
        recyclerView = findViewById(R.id.recyclerView)

        // animation
        val set = AnimationSet(true)
        val fadeIn: Animation = AlphaAnimation(0.0f, 1.0f)
        fadeIn.duration = 1000
        fadeIn.fillAfter = true
        set.addAnimation(fadeIn)
        val slideUp: Animation =
            TranslateAnimation(-300f, 0f, 0f,0f)
        slideUp.interpolator = DecelerateInterpolator(4f)
        slideUp.duration = 1000
        set.addAnimation(slideUp)
        val controller = LayoutAnimationController(set, 0.2f)

        recyclerView.setLayoutAnimation(controller)
        var adapter: TripItemAdapter = TripItemAdapter(this, items)

        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(this)

        if (isServiceOK()) {
        }

    }

    public fun initItems() {
        items.add(
            TripItem(
                "Sagbon 156 a block, Olmazor dist...",
                "Amir Temur Avenue 2a block 23 A",
                "18,200sum"
            )
        )
        items.add(TripItem("Istiqbol 20 h, Mirabad dis.", "Sharaf Rashidov, prospect", "7,700sum"))
        items.add(
            TripItem(
                "Muqimi 2a h, Chilanzar dis.",
                "Qumbuloq 13 h, Uchtepa dis.",
                "10,000sum"
            )
        )
        items.add(
            TripItem(
                "Shodlik 60 h, Uchtepa dis.",
                "Amir Temur Avenue 2a block 23 A",
                "29,800sum"
            )
        )
        items.add(
            TripItem(
                "Sagbon 156 a block, Olmazor dist...",
                "Amir Temur Avenue 2a block 23 A",
                "18,200sum"
            )
        )
        items.add(TripItem("Istiqbol 20 h, Mirabad dis.", "Sharaf Rashidov, prospect", "7,700sum"))
        items.add(
            TripItem(
                "Muqimi 2a h, Chilanzar dis.",
                "Qumbuloq 13 h, Uchtepa dis.",
                "10,000sum"
            )
        )
        items.add(
            TripItem(
                "Shodlik 60 h, Uchtepa dis.",
                "Amir Temur Avenue 2a block 23 A",
                "29,800sum"
            )
        )
        items.add(
            TripItem(
                "Sagbon 156 a block, Olmazor dist...",
                "Amir Temur Avenue 2a block 23 A",
                "18,200sum"
            )
        )
        items.add(TripItem("Istiqbol 20 h, Mirabad dis.", "Sharaf Rashidov, prospect", "7,700sum"))
        items.add(
            TripItem(
                "Muqimi 2a h, Chilanzar dis.",
                "Qumbuloq 13 h, Uchtepa dis.",
                "10,000sum"
            )
        )
        items.add(
            TripItem(
                "Shodlik 60 h, Uchtepa dis.",
                "Amir Temur Avenue 2a block 23 A",
                "29,800sum"
            )
        )

    }

    public fun init() {
        val intent = Intent(this, MapActivity::class.java)
        startActivity(intent)
        finish()
    }

    public fun isServiceOK(): Boolean {
        Log.d(TAG, "isServiceOK: checking google services version");

        val available = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(this)
        if (available == ConnectionResult.SUCCESS) {
            // everything is fine
            Log.d(TAG, "isServiceOK: Google PlayServices working fine")
            return true
        } else if (GoogleApiAvailability.getInstance().isUserResolvableError(available)) {
            // an error occured but we can solve it
            Log.d(TAG, "isServicesOK: an error occured but we can fix it")
            val dialog: Dialog = GoogleApiAvailability.getInstance()
                .getErrorDialog(this, available, ERROR_DIALOG_REQUEST)
            dialog.show()
        } else {
            Toast.makeText(this, "We can't make map requests...", Toast.LENGTH_SHORT).show()
        }
        return false
    }

}
