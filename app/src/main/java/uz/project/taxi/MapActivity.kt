package uz.project.taxi

import android.Manifest
import android.annotation.SuppressLint
import android.app.ActionBar
import android.app.PendingIntent
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.os.AsyncTask
import com.google.android.gms.location.LocationListener;
import android.os.Bundle
import android.os.CountDownTimer
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.widget.RelativeLayout
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import kotlinx.android.synthetic.main.activity_map.*
import permissions.dispatcher.OnShowRationale
import permissions.dispatcher.RuntimePermissions
import permissions.dispatcher.NeedsPermission as NeedsPermission1


@RuntimePermissions
class MapActivity : AppCompatActivity(), OnMapReadyCallback,
    LocationSource.OnLocationChangedListener {

    private val MILLISECONDS_PER_SECOND = 1000;
    public val UPDATE_INTERVAL_IN_SECONDS = 5;
    private val UPDATE_INTERVAL: Long =
        MILLISECONDS_PER_SECOND.toLong() * UPDATE_INTERVAL_IN_SECONDS;

    private val FASTEST_INTERVAL_IN_SECONDS = 1;
    private val FASTEST_INTERVAL: Long =
        MILLISECONDS_PER_SECOND * FASTEST_INTERVAL_IN_SECONDS.toLong();

    private val TAG = "MapActivity"
    val FINE_LOCATION = android.Manifest.permission.ACCESS_FINE_LOCATION;
    val COARSE_LOCATION = android.Manifest.permission.ACCESS_COARSE_LOCATION;
    private val LOCATION_PERMISSION_REQUEST_CODE = 1234;
    private val DEFAULT_ZOOM = 15f
    private var mLocationPermissionGaranted: Boolean = false;
    lateinit var mMap: GoogleMap
    private lateinit var mFusedLocationProvideCLient: FusedLocationProviderClient

    private lateinit var mLocationRequest: LocationRequest

    lateinit var footer: RelativeLayout
    var IS_SWIPED_UP: Boolean = false
    var IS_SWIPING_UP: Boolean = false
    var IS_SWIPING_DOWN: Boolean = false
    var ACTION_START: Boolean = false
    lateinit var layoutMap: RelativeLayout

    override fun onMapReady(p0: GoogleMap?) {
        if (p0 != null) {
            mMap = p0
        }

        if (mLocationPermissionGaranted) {
            Log.d("TEST", "working")
            getDeviceLocation()
            mMap.isMyLocationEnabled = false;

            mMap.uiSettings.isMyLocationButtonEnabled = true;
        }
    }


    @permissions.dispatcher.NeedsPermission(Manifest.permission.ACCESS_FINE_LOCATION)
    @SuppressLint("ClickableViewAccessibility", "RestrictedApi")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val actionBar: androidx.appcompat.app.ActionBar? = supportActionBar
        if (actionBar != null) {
            actionBar.hide()
        }
        setContentView(R.layout.activity_map)
        footer = findViewById(R.id.footer_map)
        layoutMap = findViewById(R.id.map_layout)

        getLocationPermission();
        var currentPos: Int = 100;
        var FOOTER_HEIGHT = 0
        var checker = true;

        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(UPDATE_INTERVAL);
        mLocationRequest.setFastestInterval(FASTEST_INTERVAL);
        mFusedLocationProvideCLient = FusedLocationProviderClient(this);



        footer.setOnTouchListener(View.OnTouchListener { view, event ->
            var i = footer.height.toInt()


            when (event.action) {
                MotionEvent.ACTION_DOWN -> {
                    currentPos = footer.height
                    checker = view.height - event.y > FOOTER_HEIGHT - 300
                }
                MotionEvent.ACTION_UP -> {

                    if (IS_SWIPING_UP) {
                        first_item.visibility = View.GONE
                        ACTION_START = true
                        val timer = object : CountDownTimer(500, 1) {
                            override fun onTick(millisUntilFinished: Long) {
                                val param: RelativeLayout.LayoutParams =
                                    RelativeLayout.LayoutParams(
                                        RelativeLayout.LayoutParams.MATCH_PARENT,
                                        i
                                    )
                                i += 100;
                                param.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM)
                                footer.layoutParams = param
                            }

                            override fun onFinish() {
                                val param: RelativeLayout.LayoutParams =
                                    RelativeLayout.LayoutParams(
                                        RelativeLayout.LayoutParams.MATCH_PARENT,
                                        RelativeLayout.LayoutParams.MATCH_PARENT
                                    )
                                footer.layoutParams = param
                                IS_SWIPED_UP = true
                                FOOTER_HEIGHT = footer_map.height
                            }
                        }
                        timer.start()
                        IS_SWIPING_UP = false
                    }

                    if (IS_SWIPING_DOWN) {
                        arrow_back.animate().translationX(20f).alpha(1f)
                        val timer = object : CountDownTimer(500, 10) {

                            override fun onTick(millisUntilFinished: Long) {
                                if (i > getResources().getDimension(R.dimen.footer_default_width).toInt()){
                                    alphaPage.alpha = i.toFloat() / 3000
                                    val param: RelativeLayout.LayoutParams =
                                        RelativeLayout.LayoutParams(
                                            RelativeLayout.LayoutParams.MATCH_PARENT,
                                            i
                                        )
                                    i -= 50;
                                    param.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM)
                                    footer.layoutParams = param
                                }
                            }

                            override fun onFinish() {
                                full_info_item.visibility = View.GONE
                                first_item.visibility = View.VISIBLE
                                alphaPage.alpha = 0f
                                val param: RelativeLayout.LayoutParams =
                                    RelativeLayout.LayoutParams(
                                        RelativeLayout.LayoutParams.MATCH_PARENT,
                                        getResources().getDimension(R.dimen.footer_default_width).toInt()
                                    )
                                param.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM)
                                footer.layoutParams = param
                                IS_SWIPED_UP = false
                                IS_SWIPING_DOWN = false
                            }
                        }
                        timer.start()

                    }

                }
                MotionEvent.ACTION_MOVE -> {

                    if (!IS_SWIPED_UP && Math.abs(event.getY().toInt()) / 10 + footer.height < 1200) {
                        arrow_back.animate().translationX(-20f).alpha(0f)
                        first_item.visibility = View.GONE
                        full_info_item.visibility = View.VISIBLE
                        alphaPage.alpha = (Math.abs(event.getY()) / 10 + footer.height) / 3000;
                        val param: RelativeLayout.LayoutParams = RelativeLayout.LayoutParams(
                            RelativeLayout.LayoutParams.MATCH_PARENT,
                            Math.abs(event.getY().toInt()) / 10 + footer.height
                        )
                        param.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM)

                        footer.layoutParams = param
                        IS_SWIPING_UP = true
                    }

                    if (IS_SWIPED_UP && checker) {
                        alphaPage.alpha = (Math.abs(event.getY()) / 10 + footer.height) / 3000;
                        val param: RelativeLayout.LayoutParams = RelativeLayout.LayoutParams(
                            RelativeLayout.LayoutParams.MATCH_PARENT,
                            footer.height - Math.abs(event.getY().toInt())
                        )
                        param.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM)

                        footer.layoutParams = param
                        IS_SWIPING_DOWN = true
                    }

                }
            }
            return@OnTouchListener true

        })
    }

    @SuppressLint("RestrictedApi")
    private fun checkPermissions() {


        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED &&
            ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED &&
            ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            ) == PackageManager.PERMISSION_GRANTED
        ) {

        } else {

            ActivityCompat.requestPermissions(
                this, arrayOf(
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.ACCESS_NETWORK_STATE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                ), 555
            );
        }


    }



    public fun getDeviceLocation() {
        mFusedLocationProvideCLient = LocationServices.getFusedLocationProviderClient(this)

        try {
            mFusedLocationProvideCLient.requestLocationUpdates(mLocationRequest, null)
            val location: Task<Location> = mFusedLocationProvideCLient.lastLocation
            location.addOnCompleteListener(OnCompleteListener {
                if (it.isSuccessful) {
                    Log.d(TAG, "onComplete: found location!")
                    Toast.makeText(this, "Your current location found...", Toast.LENGTH_SHORT).show()
                    val currentLocation: Location = it.getResult() as Location

                    // map marker
                    mMap.uiSettings.isRotateGesturesEnabled = false
                    mMap.addMarker(
                        MarkerOptions().flat(true).icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_point_end))
                            .anchor(0.9f, 0.9f).position(
                                LatLng(
                                    currentLocation.getLatitude(), currentLocation
                                        .getLongitude()
                                )
                            )
                    )

                    moveCamera(
                        LatLng(currentLocation.latitude, currentLocation.longitude),
                        DEFAULT_ZOOM
                    )
                } else {
                    Log.d(TAG, "onComplete: current location is null")
//                    Toast.makeText(this, "Unable to get location", Toast.LENGTH_LONG).show()
                }
            })
        } catch (e: SecurityException) {
            Log.d(TAG, "getDeviceLocation: SecurityException: ${e.message}")
        }
    }


    public fun moveCamera(latLng: LatLng, zoom: Float) {
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom))
    }


    @OnShowRationale(
        Manifest.permission.ACCESS_FINE_LOCATION,
        Manifest.permission.ACCESS_COARSE_LOCATION,
        Manifest.permission.ACCESS_NETWORK_STATE
    )
    private fun getLocationPermission() {
        //// TODO: MUST FIX BUGS...
        checkPermissions()

        mLocationPermissionGaranted = true
        initMap()
    }

    private fun initMap() {
        val mapFragment: SupportMapFragment =
            supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    @SuppressLint("NeedOnRequestPermissionsResult")
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {

        for (grantResult in grantResults){
            if (grantResult == PackageManager.PERMISSION_DENIED){
                checkPermissions()
                return ;

            }
        }
        if(requestCode == 555){

                initMap()
        }

        when (requestCode) {
            LOCATION_PERMISSION_REQUEST_CODE -> {
                if (grantResults.size > 0) {
                    for (index in grantResults.indices) {
                        if (grantResults[index] != PackageManager.PERMISSION_GRANTED) {
                            mLocationPermissionGaranted = false
                            return
                        }
                    }
                    mLocationPermissionGaranted = true
                }
            }
        }
    }

    override fun onLocationChanged(location: Location?) {
        var mPositionMarker: Marker
        if (location == null)
            return;

        mPositionMarker = mMap.addMarker(
            MarkerOptions().flat(true)
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_pin))
                .anchor(0.9f, 0.9f)
                .position(LatLng(location.latitude, location.longitude))
        )

        Animation.animateMarker(mPositionMarker, location);

        mMap.animateCamera(
            CameraUpdateFactory.newLatLng(
                LatLng(
                    location
                        .getLatitude(), location.getLongitude()
                )
            )
        )


    }

    fun go_to_list(view: View) {
        val intent: Intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
    }


}