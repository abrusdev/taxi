package uz.project.taxi;

public class TripItem {
    private String fromLocation;
    private String toLocation;
    private String price;


    public TripItem(String fromLocation, String toLocation, String price) {
        this.fromLocation = fromLocation;
        this.toLocation = toLocation;
        this.price = price;
    }

    public String getFromLocation() {
        return fromLocation;
    }

    public String getToLocation() {
        return toLocation;
    }

    public String getPrice() {
        return price;
    }
}
