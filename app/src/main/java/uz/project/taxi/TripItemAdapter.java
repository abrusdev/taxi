package uz.project.taxi;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class TripItemAdapter  extends RecyclerView.Adapter<TripItemAdapter.ViewHolder>{

    ArrayList<TripItem> items;
    Context mContext;

    public TripItemAdapter( Context mContext, ArrayList<TripItem> items) {
        this.items = items;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.trips_layout, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.fromLocation.setText(items.get(position).getFromLocation());
        holder.toLocation.setText(items.get(position).getToLocation());
        holder.price.setText(items.get(position).getPrice());
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView fromLocation;
        TextView toLocation;
        TextView price;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            fromLocation = itemView.findViewById(R.id.fromLocation_txt);
            toLocation = itemView.findViewById(R.id.toLocation_txt);
            price = itemView.findViewById(R.id.price_txt);
        }
    }

}
